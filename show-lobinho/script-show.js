function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", "../lobinhos.json", true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            var jsonData = JSON.parse(xobj.responseText);
            callback(jsonData);
        }
    };
    xobj.send(null);
}


//get query param
const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('indiceLobinho');

function showLobinho(jsonData) {
    let lobinho = jsonData[id]

    //alters name
    let nomeLobinho = document.querySelector("#nomeLobinho");
    nomeLobinho.innerHTML = lobinho.nome;

    //alters description
    let descLobinho = document.querySelector("#descLobinho");
    descLobinho.innerHTML = lobinho.descricao;

    //alters image
    let fotoLobinho = document.querySelector("#fotoLobinho");
    fotoLobinho.src = lobinho.imagem;

    let botaoAdotar = document.querySelector("#botaoAdotar");
    botaoAdotar.disabled = lobinho.adotado
}

loadJSON(showLobinho);