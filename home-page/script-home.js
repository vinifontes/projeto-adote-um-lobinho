function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", "lobinhos.json", true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            var jsonData = JSON.parse(xobj.responseText);
            callback(jsonData);
        }
    };
    xobj.send(null);
}

// Função pra aleatorizar os lobos
function setRandomBackground(jsonData) {
    var allLobos = document.querySelectorAll(".container");
    allLobos.forEach(element => {
        // random index
        var randomIndex = Math.floor(Math.random() * jsonData.length);
        var randomImage = jsonData[randomIndex].imagem;
        // imagem aleatória
        var imgDiv = element.querySelector('.foto')
        imgDiv.style.backgroundImage= `url(${randomImage})`;
        // pegar nome
        var randomName = jsonData[randomIndex].nome
        var nameDiv = element.querySelector('h3')
        nameDiv.innerHTML = randomName
        // pegar idade
        var randomAge = jsonData[randomIndex].idade
        var ageDiv = element.querySelector('.wolfAge')
        ageDiv.innerHTML = 'Idade: '+randomAge+' anos'
        // pegar descrição
        var randomDesc = jsonData[randomIndex].descricao
        var descDiv = element.querySelector('p')
        descDiv.innerHTML = randomDesc
    });
}

// Load JSON and set random background
loadJSON(setRandomBackground);